package org.bitbucket.javatek.jdbc;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 */
public final class Params {
  public static final Params NO_PARAMS = new Params();

  private final Object[] params;

  public Params(Object... params) {
    this.params = params;
  }

  void apply(PreparedStatement statement) throws SQLException {
    for (int i = 0; i < params.length; i++) {
      Object param = params[i];
      if (param == null) {
        statement.setNull(
          1 + i,
          Types.OTHER
        );
      }
      else if (param.getClass() == Boolean.class) {
        statement.setBoolean(
          1 + i,
          (boolean) param
        );
      }
      else if (param.getClass() == Integer.class) {
        statement.setInt(
          1 + i,
          (Integer) param
        );
      }
      else if (param.getClass() == String.class) {
        statement.setString(
          1 + i,
          (String) param
        );
      }
      else if (param.getClass() == Timestamp.class) {
        statement.setTimestamp(
          1 + i,
          (Timestamp) param
        );
      }
      else if (param.getClass() == LocalDateTime.class) {
        statement.setTimestamp(
          1 + i,
          Timestamp.valueOf((LocalDateTime) param)
        );
      }
      else if (param.getClass() == LocalDate.class) {
        statement.setDate(
          1 + i,
          Date.valueOf((LocalDate) param)
        );
      }
      else if (param instanceof String[]) {
        statement.setArray(
          1 + i,
          statement
            .getConnection()
            .createArrayOf("VARCHAR", (Object[]) param)
        );
      }
      else {
        statement.setString(
          1 + i,
          param.toString()
        );
      }
    }
  }
}
