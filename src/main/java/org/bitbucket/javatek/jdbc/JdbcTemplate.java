package org.bitbucket.javatek.jdbc;

import org.intellij.lang.annotations.Language;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 *
 */
public final class JdbcTemplate {

  private final DataSource dataSource;

  public JdbcTemplate(DataSource dataSource) {
    this.dataSource = requireNonNull(dataSource);
  }

  @Nullable
  public <E> E querySingle(@Language("SQL") String query, Mapper<E> mapper) throws SQLException {
    return querySingle(query, Params.NO_PARAMS, mapper);
  }

  @Nullable
  public <E> E querySingle(@Language("SQL") String query, Params params, Mapper<E> mapper) throws SQLException {
    List<E> list = queryList(query, params, mapper);
    switch (list.size()) {
      case 0:
        return null;
      case 1:
        return list.iterator().next();
      default:
        throw new SQLException("Query returned too many rows");
    }
  }

  public <E> List<E> queryList(@Language("SQL") String query, Mapper<E> mapper) throws SQLException {
    return queryList(query, Params.NO_PARAMS, mapper);
  }

  public <E> List<E> queryList(@Language("SQL") String query, Params params, Mapper<E> mapper) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      connection.setReadOnly(true);
      try (PreparedStatement stmt = connection.prepareStatement(query)) {
        params.apply(stmt);
        try (ResultSet rs = stmt.executeQuery()) {
          List<E> list = new ArrayList<>();
          while (rs.next()) {
            list.add(mapper.map(rs));
          }
          return list;
        }
      }
      catch (SQLException | RuntimeException e) {
        try {
          connection.rollback();
        }
        catch (SQLException | RuntimeException e2) {
          e.addSuppressed(e2);
        }
        throw e;
      }
    }
  }

  public int execute(@Language("SQL") String query) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      connection.setReadOnly(false);
      try (PreparedStatement stmt = connection.prepareStatement(query)) {
        return stmt.executeUpdate();
      }
      catch (SQLException | RuntimeException e) {
        try {
          connection.rollback();
        }
        catch (SQLException | RuntimeException e2) {
          e.addSuppressed(e2);
        }
        throw e;
      }
    }
  }

  public int execute(@Language("SQL") String query, @Nonnull Params params) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      connection.setReadOnly(false);
      try (PreparedStatement stmt = connection.prepareStatement(query)) {
        params.apply(stmt);
        return stmt.executeUpdate();
      }
      catch (SQLException | RuntimeException e) {
        try {
          connection.rollback();
        }
        catch (SQLException | RuntimeException e2) {
          e.addSuppressed(e2);
        }
        throw e;
      }
    }
  }

  public int executeBatch(@Language("SQL") String query, Batcher batcher) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      connection.setReadOnly(false);
      try (PreparedStatement stmt = connection.prepareStatement(query)) {
        batcher.pullBatches(new Batches(stmt));
        int[] updates = stmt.executeBatch();
        connection.commit();

        int updated = 0;
        for (int update : updates) {
          updated += update;
        }
        return updated;
      }
      catch (SQLException | RuntimeException e) {
        try {
          connection.rollback();
        }
        catch (SQLException | RuntimeException e2) {
          e.addSuppressed(e2);
        }
        throw e;
      }
    }
  }
}
