package org.bitbucket.javatek.jdbc;

import javax.annotation.Nonnull;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 */
@FunctionalInterface
public interface Mapper<T> {
  @Nonnull
  T map(@Nonnull ResultSet rs) throws SQLException;
}
