package org.bitbucket.javatek.jdbc;

import javax.annotation.Nonnull;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 */
public final class Batches {
  private final PreparedStatement statement;

  Batches(PreparedStatement statement) {
    this.statement = statement;
  }

  public void addBatch(Object... params) throws SQLException {
    addBatch(new Params(params));
  }

  public void addBatch(@Nonnull Params params) throws SQLException {
    statement.clearParameters();
    params.apply(statement);
    statement.addBatch();
  }
}
