package org.bitbucket.javatek.jdbc;

import java.sql.SQLException;

/**
 *
 */
@FunctionalInterface
public interface Batcher {
  void pullBatches(Batches batches) throws SQLException;
}
